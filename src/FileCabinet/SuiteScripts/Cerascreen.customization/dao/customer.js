define(
	['N/search'],
	function (search) {

		function customerIsIndividual (customerId) {

			let customerObj = search.lookupFields({
                type: search.Type.CUSTOMER,
                id: customerId,
                columns: ['isperson']
            });

			return customerObj.isperson;
		}

		return {
			customerIsIndividual,
		};
	}
);
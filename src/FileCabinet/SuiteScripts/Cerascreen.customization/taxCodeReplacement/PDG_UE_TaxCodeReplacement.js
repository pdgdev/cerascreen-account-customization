/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 */
define([
        'N/runtime',
        '../common/constants',
        '../dao/customer',
    ],

    (runtime,
            constants,
            customerDAO
    ) => {

        /**
         * Defines the function definition that is executed before record is submitted.
         * @param {Object} scriptContext
         * @param {Record} scriptContext.newRecord - New record
         * @param {Record} scriptContext.oldRecord - Old record
         * @param {string} scriptContext.type - Trigger type; use values from the context.UserEventType enum
         * @since 2015.2
         */
        const beforeSubmit = (scriptContext) => {

            if (scriptContext.type !== scriptContext.UserEventType.CREATE) {
                return;
            }

            let salesOrder = scriptContext.newRecord;
            let subsidiary = salesOrder.getValue('subsidiary');
            let customerId = salesOrder.getValue('entity');

            let customerIsIndividual = customerDAO.customerIsIndividual(customerId);

            if (customerIsIndividual) {
                let scriptObj = runtime.getCurrentScript();
                let subsidiaryForReplacement = scriptObj.getParameter(constants.SCRIPT_PARAMS.UE_TAX_REPLACEMENT.SUBSIDIARY_FOR_TAX_REPLACEMENT);

                if (subsidiary !== subsidiaryForReplacement) {
                    return;
                }

                for (let i = 0; i < salesOrder.getLineCount('item'); i++) {
                    let taxCodeForReplacement = salesOrder.getSublistValue({
                        sublistId: 'item',
                        fieldId: constants.CUSTOM_FIELDS.TRANSACTION_LINE.TAX_CODE_REPLACEMENT,
                        line: i,
                    });

                    if (taxCodeForReplacement) {
                        salesOrder.setSublistValue({
                            sublistId: 'item',
                            fieldId: 'taxcode',
                            value: taxCodeForReplacement,
                            line: i,
                        });
                    }
                }
            }
        }

        return {beforeSubmit}

    });

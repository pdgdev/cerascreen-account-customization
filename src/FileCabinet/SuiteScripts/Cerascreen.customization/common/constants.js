define([], function () {
    return Object.freeze({
        CUSTOM_FIELDS: {
            TRANSACTION_LINE: {
                TAX_CODE_REPLACEMENT: 'custcol_pdg_tax_code_for_replacement',
            },
            ITEM: {
                TAX_CODE_REPLACEMENT: 'custitem_pdg_tax_code_for_replacement',
            },
        },
        SCRIPT_PARAMS: {
            UE_TAX_REPLACEMENT: {
                SUBSIDIARY_FOR_TAX_REPLACEMENT: 'custscript_pdg_sub_for_tax_code_replace',
            },
        },
    });
});